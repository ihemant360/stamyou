<?php



function stamyou_widget_support(){

register_sidebar(array(

			'name'=>'Left-Sidebar',
			'id'=>'l-sidebar'

		));


}
add_action('widgets_init','stamyou_widget_support');


function stamyou_theme_support(){

	add_theme_support('post-thumbnails');

}
add_action('after_setup_theme' , 'stamyou_theme_support');


function assets_links(){


	wp_enqueue_style( 'mian-style' , get_stylesheet_uri() );

}
add_action( 'wp_enqueue_scripts' , 'assets_links');