<?php get_header(); ?>

<div class="main-container">

<div class="sidebar">

	<?php get_sidebar('l-sidebar'); ?>

</div>

<div class="content">

	<?php if ( have_posts() ) : ?>
		<?php while( have_posts() ) : the_post(); ?>
		<?php get_template_part('template-parts/content' , get_post_format() ); ?>
		<?php endwhile; ?>
	<?php else: ?>
			<h2>Sorry NO Post Found !</h2>
	<?php endif; ?>

</div>

</div>



<?php get_footer(); ?>

