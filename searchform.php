<form role="search" method="get"  action="<?php echo home_url( '/' ); ?>">

    <label>
        <div class="search-form">
        <input type="search" 
            placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
            value="<?php echo get_search_query() ?>" name="s"
            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
        </div>
    </label><!--
    <input type="submit" class="search-submit btn btn-primary"
        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
-->
</form>