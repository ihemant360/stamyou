<?php 
/*
	Main Template File for displaying Blog posts

*/
?>


<h1><a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></h1>
<small>
<?php the_time( 'F j, Y g:i a' )?> | By
	<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a> |
		<?php
			$categories= get_the_category();
			$separator = ",";
			$output    = '';

			if( $categories ){
				foreach( $categories as $category ){
					$output .=
					'<a href ="'. get_category_link( $category->term_id ) .'">'. $category->cat_name .'</a>' . $separator;
				}
				echo trim( $output , $separator );
			}
		?>
</small>
<div class="f-image">

	<?php if ( has_post_thumbnail() ): ?>
            <?php $featured_image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
            <img src="<?php echo $featured_image; ?>" />
  <?php endif; ?>

</div>
<?php the_content(); ?>